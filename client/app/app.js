import angular from 'angular';
import uiRouter from 'angular-ui-router';
import Common from './common/common';
import Components from './components/components';
import AppComponent from './app.component';
import ngMessages from 'angular-messages';
import firebase from 'firebase';
import 'angularfire';
import 'bootstrap/dist/css/bootstrap.css';

import 'amcharts/dist/amcharts/amcharts.js';
import 'amcharts/dist/amcharts/serial.js';
import 'amcharts/dist/amcharts/gauge.js';
import 'amcharts/dist/amcharts/pie.js';
import 'amcharts/dist/amcharts/themes/light.js';
import  'angular-ui-grid/ui-grid.js';
import  'angular-ui-grid/ui-grid.css';

import 'ui-select/dist/select.js';
import 'ui-select/dist/select.css';
import 'auth0-angular';
import angularJwt from 'angular-jwt';
import angularStorage from 'angular-storage';
import AuthService from './common/auth/auth.factory';
import angularUiBootstrap from 'angular-ui-bootstrap';
import 'angular-ui-bootstrap-datetimepicker';
import 'angular-pageslide-directive/dist/angular-pageslide-directive.js';
import authorizationService from './common/authorization_service/authorization_service';


angular.module('app', [
    ngMessages,
    uiRouter,
    Common,
    Components,
    'firebase',
    'ui.select',
    'auth0',
    angularJwt,
    angularStorage,
    'ui.bootstrap'
  ])
  .service('authorizationService',authorizationService)
  .config(['$locationProvider', 'authProvider', '$stateProvider', ($locationProvider, authProvider, $stateProvider) => {

    $locationProvider.html5Mode(true);

    authProvider.init({
      domain: 'admin-panel.eu.auth0.com',
      clientID: 'tbWq0c8fs7qZozb4Ke1cSnVlrn4SbwGS'
    });

    firebase.initializeApp({
    apiKey: "AIzaSyB67d-gNSW9hp3kdG0-pl0xhgtX83fjzxo",
    authDomain: "userlistdatabase.firebaseapp.com",
    databaseURL: "https://userlistdatabase.firebaseio.com",
    storageBucket: "userlistdatabase.appspot.com",
    messagingSenderId: "509232754685"
    });

  }])

  .component('app', AppComponent)
  .factory('AuthService', AuthService)
  .run(['$state', 'AuthService', ($state, AuthService) => {
    if(!AuthService.auth()){
      AuthService.loc($state);
    }
  }]);