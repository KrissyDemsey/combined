class NavbarController {
  constructor($state, store){
    'ngInject';
    this.state = $state;
    this.store = store;
  }
  $onInit(){
    this.links= [
      {name: 'Home',      link: "home", id:1},
      {name: 'Search',    link: "search", id:2},
      {name: 'User List', link: 'userlist',id:3},
      {name: 'Statistic', link: "statistic", id:4}
    ];
  }
  profile(){
    return this.store.get('profile');
  }
  logout(){
    this.store.remove('profile');
    this.store.remove('access');
    this.store.remove('token');
    this.state.go('login');
    localStorage.clear();
  }

  access(){
    return localStorage.access;
  }
};

export default NavbarController;
