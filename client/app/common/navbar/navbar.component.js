import template from './navbar.html';
import controller from './navbar.controller';
import './navbar.css';

const navbarComponent = {
  bindings: {},
  template,
  controller
};

export default navbarComponent;





