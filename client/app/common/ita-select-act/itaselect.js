import angular from 'angular';
import uiRouter from 'angular-ui-router';
import itaSelectComponent from './itaselect.component';
import './itaselect.styl';
import './itaselect.css';

import 'ui-select/dist/select.js';
import 'ui-select/dist/select.css';

const itaSelectModule = angular.module('itaSelectsite', [
  'ui.grid'
])

.component('itaSelectsite', itaSelectComponent)
  
.name;

export default itaSelectModule;
