import angular from 'angular';
import uiRouter from 'angular-ui-router';
import itaSelectComponent from './itaselect.component';
import './itaselect.styl';
import 'ui-select/dist/select.js';
import 'ui-select/dist/select.css';

const itaSelectModule = angular.module('itaSelect', ['ui.grid'])

.component('itaSelect', itaSelectComponent)
  
.name;

export default itaSelectModule;
