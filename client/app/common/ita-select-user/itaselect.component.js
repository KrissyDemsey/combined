import template from './itaselect.html';
import controller from './itaselect.controller';

const itaSelectComponent = {
  bindings: {
    config:'<',
    users:'<',
    onUpdate: '&'
  },
  template,
  controller
};

export default itaSelectComponent;
