import angular from 'angular';
import Navbar from './navbar/navbar';
import ItaSelect from './ita-select-user/itaselect';
import ItaSelectSite from './ita-select-act/itaselect';
import Timedatepicker from './timedatepicker/timedatepicker';

const CommonModule = angular

.module('app.common', [
  Navbar,
  ItaSelect,
  ItaSelectSite,
  Timedatepicker
]).name;

export default CommonModule;
