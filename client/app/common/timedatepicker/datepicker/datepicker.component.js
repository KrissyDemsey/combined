import template from './datepicker.html';
import controller from './datepicker.controller';

const datepickerComponent = {
  bindings: {
    date:'<',
    onUpdate: '&'
  },
  template,
  controller
};

export default datepickerComponent;