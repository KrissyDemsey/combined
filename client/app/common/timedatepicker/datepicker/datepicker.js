import angular from 'angular';
import uiRouter from 'angular-ui-router';
import datepickerComponent from './datepicker.component';

const datepickerModule = angular
  .module('datepicker', [uiRouter])
  .component('datepicker', datepickerComponent)
  .name;

export default datepickerModule;