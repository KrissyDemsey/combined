import template from './timepicker.html';
import controller from './timepicker.controller';

const timepickerComponent = {
  bindings: {
    time:'<',
    onUpdate: '&'
  },
  template,
  controller
};

export default timepickerComponent;