import angular from 'angular';
import uiRouter from 'angular-ui-router';
import statisticComponent from './statistic.component';
import ItaCharts from './itaCharts/ita';
import amchRender from './statistic.amchRender.service .js';


const statisticModule = angular.module('statistic', [
  uiRouter,
  ItaCharts
])

.config(($stateProvider) => {
  "ngInject";
  $stateProvider
    .state('statistic', {
      url: '/statistic',
      component: 'statistic'
    });
})
.service('amchRender',amchRender)

.component('statistic', statisticComponent)

.name;

export default statisticModule;
