import firebase from 'firebase';

class StatisticController {
  constructor($firebaseObject,$firebaseArray,$state,amchRender) {
    const rootRef = firebase.database().ref();
    this.array = $firebaseArray(rootRef);
    this.amchartsDB = $firebaseArray(rootRef.child('/amcharts'));
    this.buildChart = false;
    this.amch = amchRender.getResults( this.amchartsDB);
    this.amchTwo = amchRender.getResultTwo(this.amchartsDB);
    this.amchThree = amchRender.getResultThree(this.amchartsDB);
  }
    $onInit  = () => {
      this.progresschart =this.amch;
      this.piechart = this.amchTwo;
      this.profitschart =  this.amchThree;
        this.amchartsDB.$loaded().then(() => {
        this.buildChart = true;
      });
    };
}
StatisticController['$inject'] = ['$firebaseObject','$firebaseArray', '$state','amchRender'];
export default StatisticController;