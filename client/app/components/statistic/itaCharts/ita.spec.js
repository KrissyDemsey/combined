import ChartModule from './ita';
import ChartComponent from './ita.component';
import ChartTemplate from './ita.html';

describe('Chart', () => {
  const $rootScope;
  beforeEach(window.module(ChartModule));
  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
    makeController = () => {
      return new ChartController();
    };
  }));

describe('Template', () => {
  it('expected template are present ', () => {
    expect(ChartTemplate).to.match(`<div style="width:{{$ctrl.ctrl.width}};
      height:{{$ctrl.ctrl.height}};
      margin:10px;border:1px solid lightgray;
      background:{{$ctrl.ctrl.background}};" >
      <am-chart id="{{$ctrl.ctrl.id}}" options="$ctrl.ctrl.amChartOptions">
      </am-chart>
       </div>`);
    });
  });

describe('Component', () => {
  const component = ChartComponent;
    it('includes the intended template',() => {
      expect(component.template).to.equal(ChartTemplate);
    });
  });
});