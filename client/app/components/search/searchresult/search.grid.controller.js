import firebase from 'firebase';
import angular from 'angular';
import 'angular-ui-bootstrap';

class SearchGridController {

  constructor($stateParams, SEARCHRESULTTABLE,$state,$firebaseArray) {
    
    this.getUsers = $stateParams.name.selectedItem;
    this.getUrls = $stateParams.url.selectedItem;
    this.getTimeFrom = $stateParams.timefrom;
    this.getTimeTo = $stateParams.timeto;
    this.fullDB = $stateParams.fullDB;
    this.urlDB = $stateParams.urlDB;

    let selectedUsers= this.chooseUsersToFilter();
    let selectedUrls = this.chooseUrlsToFilter();
    let selectedTimeFrom = this.getTimeFrom.getTime();
    let selectedTimeTo = this.getTimeTo.getTime();
    let urlArray = this.retypeSelectedUrlsArray(selectedUrls);

  const clearOneUserByURL =(User)=>{
    let selectedUrl = urlArray;
    let userFile = User;
    let array = userFile.visits;
    let filteredArray =array.filter((item)=>(item.url === selectedUrl[0] || item.url === selectedUrl[1] || item.url === selectedUrl[2] || item.url === selectedUrl[3]|| item.url === selectedUrl[4]|| item.url === selectedUrl[5]))
      return {name : userFile.name, visits : filteredArray}
}; 
  const clearUserArrayByUrl=(array)=>{
	  return array.map(clearOneUserByURL);
};
  const clearByTimeOneUrl = (array)=>{
    let start= selectedTimeFrom;
    let finish= selectedTimeTo;
    let name = array.url;
    let conections= array.conections;
    let filtered = conections.filter((elem) => (elem.date>start && elem.date<finish));
    let clearedItem = {
  	  url : name,
  	  conections : filtered
  }
  return clearedItem;
};
  const clearByTimeOneUser = (user) =>{
    let userfile= user;
    let name = userfile.name
    let userVisits= userfile.visits;
    let clear = userVisits.map(clearByTimeOneUrl);
    let cleanUser = {
      name : name,
      visits : clear
  }
  return cleanUser;
};
  const clearUserArrayByTime =(array)=>{
    return array.map(clearByTimeOneUser);
}

  const clearUserArrayByDate=(array)=>{
    return array.map(clearByTimeOneUser);
}
let filteredUsersArrayByUrls = clearUserArrayByUrl(selectedUsers);
let filteredArrayByDate = clearUserArrayByDate(filteredUsersArrayByUrls);

  this.serchResultTable = {
	  infiniteScrollRowsFromEnd: 21,
    infiniteScrollUp: true,
    infiniteScrollDown: true,
	  enableSorting: true,
	  enableFiltering: true,
	  enableColumnResizing: true,
	  enablePinning: true,
	  data: filteredArrayByDate,
	  columnDefs:[
  { name : "name", field: "name",cellClass: "text-center", width: 250},
  { name: "quantity_visits_domain#1", 
    displayName:"#", 
    cellClass: "text-center",
    field: "visits[0].conections.length", 
    width: 80, 
    cellTooltip: (row, col) => {
      return '  How many times '+row.entity.name+ " visit this site";},
	   },
  { name: "domain#1",
    displayName:"Domain", 
    field: "visits[0].url.toString()",
    width: 250 ,
    cellTooltip: (row, col) => {
      return row.entity.name+'  walking here' ;},
    cellClass: 'red'},
  { name: "quantity_visits_domain#2", 
    displayName:"#", 
    field: "visits[1].conections.length", 
    width: 80, 
    cellTooltip: (row, col) => {
      return '  How many times '+row.entity.name+ " visit this site" ;}},
  { name: "domain#2",
    displayName:"Domain", 
    field: "visits[1].url.toString()",
    width: 250 ,
    cellTooltip: (row, col) => {
      return row.entity.name+'  walking here';},
     cellClass: 'red'},
  { name: "quantity_visits_domain#3",
    displayName:"#",
    field: "visits[2].conections.length",
    cellClass: "text-center",
    width: 80,
    cellClass: "text-center", 
    cellTooltip: (row, col) => {
      return '  How many times '+row.entity.name+ " visit this site";}},
   { name: "domain#3",
     displayName:"Domain", 
     field: "visits[2].url.toString()", 
     width: 250 ,
     cellClass: "text-center",
     cellTooltip: (row, col) => {
       return row.entity.name+'  walking here';}},
   { name: "quantity_visits_domain#4",
     displayName:"#", field: "visits[3].conections.length", 
     cellClass: "text-center",
     width: 80,
     cellClass: "text-center", 
     cellTooltip: (row, col) => {  
       return '  How many times '+row.entity.name+ " visit this site";}},
   { name: "domain#4",
     displayName:"Domain", 
     field: "visits[3].url.toString()",
     width: 250 ,
     cellTooltip: (row, col) => {
       return row.entity.name+'  walking here';}},
   { name: "quantity_visits_domain#5",
     displayName:"#", field: "visits[4].conections.length", 
     cellClass: "text-center",
     width: 80, 
     cellTooltip: (row, col) => {  
       return '  How many times '+row.entity.name+ " visit this site";}},
   { name: "domain#5",
     displayName:"Domain", 
     field: "visits[4].url.toString()",
     width: 250 ,
     cellClass: "text-center",
     cellTooltip: (row, col) => {
       return row.entity.name+'  walking here';}
     }
  ]};
}
chooseUsersToFilter(){
  return (typeof this.getUsers =='object')? this.getUsers :  this.fullDB;
}
chooseUrlsToFilter(){
  return (typeof this.getUrls== 'object')? this.getUrls : this.urlDB;
}
retypeSelectedUrlsArray(array){
  return array.map(elem => elem = elem.url);
}}


SearchGridController['$inject'] = ['$stateParams', 'SEARCHRESULTTABLE','$state','$firebaseArray'];
export default SearchGridController;




