import template from './search.grid.html';
import controller from './search.grid.controller';


const SearchGridComponent = {
  bindings: {},
  template,
  controller
};

export default SearchGridComponent;
