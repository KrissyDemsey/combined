
const searchResultTable = [
  { name : "name", field: "name", width: 300},
  { name: "quantity_visits_domain#1", 
    displayName:"#", 
    field: "visits[0].conections.length", 
    width: 40, 
    cellTooltip: (row, col) => {
      return '  How many times '+row.entity.name+ " visit this site";},
	   },
  { name: "domain#1",
    displayName:"Domain", 
    field: "visits[0].url.toString()",
    width: 250 ,
    cellTooltip: (row, col) => {
      return row.entity.name+'  walking here' ;},
    cellClass: 'red'},
  { name: "quantity_visits_domain#2", 
    displayName:"#", 
    field: "visits[1].conections.length", 
    width: 40, 
    cellTooltip: (row, col) => {
      return '  How many times '+row.entity.name+ " visit this site" ;}},
  { name: "domain#2",
    displayName:"Domain", 
    field: "visits[1].url.toString()",
    width: 250 ,
    cellTooltip: (row, col) => {
      return row.entity.name+'  walking here';},
     cellClass: 'red'},
  { name: "quantity_visits_domain#3",
    displayName:"#",
    field: "visits[2].conections.length",
    width: 40, 
    cellTooltip: (row, col) => {
      return '  How many times '+row.entity.name+ " visit this site";}},
   { name: "domain#3",
     displayName:"Domain", 
     field: "visits[2].url.toString()", 
     width: 250 ,
     cellTooltip: (row, col) => {
       return row.entity.name+'  walking here';}},
   { name: "quantity_visits_domain#4",
     displayName:"#", field: "visits[3].conections.length", 
     width: 40, 
     cellTooltip: (row, col) => {  
       return '  How many times '+row.entity.name+ " visit this site";}},
   { name: "domain#4",
     displayName:"Domain", 
     field: "visits[3].url.toString()",
     width: 250 ,
     cellTooltip: (row, col) => {
       return row.entity.name+'  walking here';}}
];

export default searchResultTable;