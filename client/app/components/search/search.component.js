import template from './search.html';
import controller from './search.controller';
import firebase from 'angularfire';

const searchComponent = {
  bindings: {},
  template,
  controller
};

export default searchComponent;