import template from './authorization.html';
import './authorization.styl';
import controller from './authorization.controller';


const authorizationComponent = {
  template,
  controller
};

export default authorizationComponent;
