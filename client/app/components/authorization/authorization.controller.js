class AuthorizationController {
  constructor(authorizationService) {
    "ngInject";
    this.sendData = authorizationService;
  }
  $onInit(){
    this.userData = {
      userName: '',
      userEmail: '',
      userRole: '',
      userDetails: ''
    };
  }
     submit =()=> {
    if(this.userlogin.$valid) {
      this.sendData.setData(this.userData);
    }
  };
 }
AuthorizationController['$inject'] = ['authorizationService'];
export default AuthorizationController;
