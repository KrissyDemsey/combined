class SettingsController {
  constructor() {}

  toggle() {
    this.showSlide = !this.showSlide;
  };

  save2(newSettings) {
    this.save({newSettings : newSettings});
  }
}

export default SettingsController;