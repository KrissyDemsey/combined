import angular from 'angular';
import slideComponent from './pageSlide.component';

const slideModule = angular.module('slide', [
  'pageslide-directive'
])

.component('pageSlide', slideComponent)

.name;

export default slideModule;