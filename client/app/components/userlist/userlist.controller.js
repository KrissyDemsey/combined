import firebase from 'firebase';

class UserlistController {
  constructor($firebaseObject,$firebaseArray,$timeout) {
    this.firebaseArray = $firebaseArray;
    this.timeout = $timeout;
  }

  $onInit() {
    const rootRef = firebase.database().ref();
    this.array = this.firebaseArray(rootRef);
    this.dateFormat = localStorage.getItem("dateFormat") || 'MMMM d, y';
    this.rowsNumber = localStorage.getItem("numberOfTableRows") || 10;
    this.isTableMounted = true;
  }

  updateUiGrid() {
    this.isTableMounted = false;
    this.timeout(() => this.isTableMounted = true);
  }

  settingsChanged(newSettings) {
    this.rowsNumber = newSettings.rowsNumber;
    this.dateFormat = newSettings.dateFormat;
    this.updateUiGrid();
  }

  clearLocStorage() {
    localStorage.clear();
    localStorage.access = true;
  };
}

UserlistController ["$inject"]= ['$firebaseObject','$firebaseArray','$timeout'];

export default UserlistController;