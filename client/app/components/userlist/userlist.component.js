import template from './userlist.html';
import controller from './userlist.controller';
import './userlist.styl';

const userlistComponent = {
  controller,
  template
 };

export default userlistComponent;