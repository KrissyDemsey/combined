class DateController {
  constructor($filter) {
    this.filter = $filter;
  }

  $onChanges(changesObj) {
    this.date = this.filter('date')(new Date(), changesObj.format.currentValue);
  };
}

DateController['$inject'] = ['$filter'];

export default DateController;