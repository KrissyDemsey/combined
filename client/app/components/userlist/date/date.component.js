import template from './date.html';
import controller from './date.controller';
import './date.styl';

const dateComponent = {
  bindings: {
    format: '<'
  },
  template,
  controller
};

export default dateComponent;