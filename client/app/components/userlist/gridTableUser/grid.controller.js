import firebase from 'firebase';
import edittemplate from './editmodal.html';
import deletetemplate from './deletemodal.html';
import controller from './modal.controller';

class GridController {
  constructor($firebaseObject,$firebaseArray, HEADERS, $uibModal) {
    const rootRef = firebase.database().ref();
    let ctrl = this;
    this.usersDB = $firebaseArray(rootRef.child('/usersDBI'));
    this.buildGrid = false;
    this.usersDB.$loaded().then((data) => {
      this.usersTable.data = data;
      this.buildGrid = true;
    });

    ctrl.$onInit  = () => {
    this.usersTable = {
      infiniteScrollRowsFromEnd: 21,
      infiniteScrollUp: true,
      infiniteScrollDown: true,
      enableSorting: true,
      enableFiltering: true,
      showGridFooter: true,
      enableGridMenu: true,
      minRowsToShow: this.rows,
      columnDefs: HEADERS.data
      };
    }
    this.$uibModal = $uibModal;
  }
  deleteUser(row, users){
    this.$uibModal.open({
      template: deletetemplate,
      controller: controller,
      controllerAs: 'modalctrl',
      resolve: {
        row: () => {
          return row;
        },
        users: () => {
          return this.usersDB;
        }
      }
    });
  }
  edit(row, users){
    this.$uibModal.open({
      bindToController: true,
      template: edittemplate,
      controller: controller,
      controllerAs: 'modalctrl',
      resolve: {
        row: () => {
          return row;
        },
        users: () => {
          return this.usersDB;
        }
      }
    });
  }
}

GridController['$inject'] = ['$firebaseObject','$firebaseArray', 'HEADERS', '$uibModal'];

export default GridController;
