import angular from 'angular';
import uiRouter from 'angular-ui-router';
import gridComponent from './grid.component';
import firebase from 'firebase';
import 'angularfire';
import headers from './headers'; 

const gridModule = angular

.module('grid', [
  'ui.grid', 
  'ui.grid.infiniteScroll',
  'firebase'
])

.constant('HEADERS', {data: headers})

.component('userGrid', gridComponent)
  
.name;

export default gridModule;
