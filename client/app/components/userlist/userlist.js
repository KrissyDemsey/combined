import angular from 'angular';
import uiRouter from 'angular-ui-router';
import firebase from 'firebase';
import 'angularfire';
import userlistComponent from './userlist.component';
import GridTableUser from './gridTableUser/grid';
import additionalSettings from './additional_settings/index';
import date from './date/index'

const userlistModule = angular.module('userlist', [
  'firebase',
  uiRouter,
  GridTableUser,
  additionalSettings,
  date
])

.config(($stateProvider) => {
  "ngInject";
  $stateProvider
    .state('userlist', {
      url: '/userlist',
      component: 'userlist'
  });
})

.component('userlist', userlistComponent)

.name;

export default userlistModule;