import template from './authorization_file.html';
import controller from './authorization_file.controller';
import './authorization_file.styl';

const fileComponent = {
  template,
  controller
};

export default fileComponent;