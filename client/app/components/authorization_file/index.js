import angular from 'angular';
import uiRouter from 'angular-ui-router';
import fileComponent from './authorization_file.component';

const fileModule = angular.module('file', [
  uiRouter
])

.config(($stateProvider) => {
  "ngInject";

  $stateProvider
    .state('file', {
      url: '/authorization-file',
      component: 'file'
    })
})

.component('file', fileComponent)
  
.name;

export default fileModule;