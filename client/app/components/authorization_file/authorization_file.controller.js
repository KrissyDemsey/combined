class FileController {
  constructor($scope) {
    this.imageContent = null;
    this.scope = $scope;
  }

  uploadImage(input) {
      const fileObject = input.files;
      if (fileObject && fileObject[0]) {
        const reader = new FileReader();

        reader.onload = (e) => {
          this.scope.$evalAsync(() => {
            this.imageContent = e.target.result;
            
            localStorage.setItem('image',this.imageContent);
          });
        }

        reader.readAsDataURL(input.files[0]);
      }
    }
}

FileController['$inject'] = ['$scope'];

export default FileController;
