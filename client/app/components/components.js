import angular from 'angular';
import firebase from 'angularfire';
import Home from './home/home';
import Statistic from './statistic/statistic';
import Login from './login/login';
import Search from './search/search';
import Userlist from './userlist/userlist';
import Authorization from './authorization/index';
import AuthorizationPassword from './authorization_password/index';
import AuthorizationFile from './authorization_file/index';
import AuthorizationSave from './authorization_save/index';

const componentModule = angular.module('app.components',[
  firebase,
  Home,
  Login,
  Statistic,
  Search,
  Userlist,
  Authorization,
  AuthorizationPassword,
  AuthorizationFile,
  AuthorizationSave
]).name;

export default componentModule;
