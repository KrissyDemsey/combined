import template from './authorization_save.html';
import controller from './authorization_save.controller';

const saveComponent = {
  template,
  controller
};

export default saveComponent;
