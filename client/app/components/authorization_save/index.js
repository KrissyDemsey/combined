import angular from 'angular';
import uiRouter from 'angular-ui-router';
import saveComponent from './authorization_save.component';
import ResultsFinder from './authorization_showData_service';

const saveModule = angular.module('save', [
  uiRouter
])


.config(($stateProvider) => {
  "ngInject";
  $stateProvider
    .state('save', {
      url: '/authorization-save',
      component: 'save'
    })
})
  
.service('ResultsFinder',ResultsFinder)
.component('save', saveComponent)

.name;

export default saveModule;
